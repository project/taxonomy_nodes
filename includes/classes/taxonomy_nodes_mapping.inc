<?php

/**
 * @file
 * Class handling all mapping functionality
 */



define('LOADING_NO_RESULTS', 1);
define('LOADING_PARAMETERS_NOT_SET', 2);
define('SAVING_PARAMETERS_NOT_SET', 3);
define('SAVING_INSERT_FAILED', 4);
define('SAVING_UPDATE_FAILED', 5);
define('DELETE_PARAMETERS_NOT_SET', 6);
define('DELETE_FAILED', 7);

class taxonomy_nodes_mapping {
  public $id;
  public $node;
  public $vocab;
  public $error;



  public function  __construct() {
    $this->error = FALSE;
    $this-> id = FALSE;
    $this->node = FALSE;
    $this->vocab = FALSE;
  }



  public function get($id) {
    $sql = "SELECT id, node_type AS node, vid AS vocab FROM {taxonomy_nodes_mapping} WHERE id = %d";
    $result = db_fetch_object(db_query($sql, $id));

    if (db_affected_rows() > 0) {
      $this->id = $id;
      $this->node = $result->node;
      $this->vocab = $result->vocab;
    }
    else $this->error = LOADING_NO_RESULTS;
  }



  public function get_id() {
    $sql = "SELECT id FROM {taxonomy_nodes_mapping} WHERE node_type = '%s' AND vid = %d";
    $result = db_result(db_query($sql, $this->node, $this->vocab));

    if ($this->node && $this->vocab) {
      (db_affected_rows() > 0)?$this->id = $result:$this->error = LOADING_NO_RESULTS;
    }
    else $this->error = LOADING_PARAMETERS_NOT_SET;
  }



  public function save() {
    if ($this->node && $this->vocab) {
      $this->get_id();
      ($this->id)?$this->update():$this->insert();
    }
    else $this->error = SAVING_PARAMETERS_NOT_SET;
  }



  private function insert() {
    $sql = "INSERT INTO {taxonomy_nodes_mapping} (node_type, vid) VALUES ('%s', %d)";
    (db_query($sql, $this->node, $this->vocab))?$this->error = FALSE:$this->error = SAVING_INSERT_FAILED;
  }



  private function update() {
    $sql = "UPDATE {taxonomy_nodes_mapping} SET node_type = '%s', vid = %d WHERE id = %d";
    (db_query($sql, $this->node, $this->vocab, $this->id))?$this->error = FALSE:$this->error = SAVING_UPDATE_FAILED;
  }



  public function delete() {
    if ($this->id) {
      $sql = "DELETE FROM {taxonomy_nodes_mapping} WHERE id = %d";
      (db_query($sql, $this->id))?$this->error = FALSE:$this->error = DELETE_FAILED;
    }
    else $this->error = DELETE_PARAMETERS_NOT_SET;
  }



  public static function get_all($node_type = NULL) {
    $results = FALSE;
    $sql = "SELECT id FROM {taxonomy_nodes_mapping}";
    if (!empty($node_type)) {
      $sql .= " WHERE node_type = '%s'";
      $sql = db_query($sql, $node_type);
    }
    else $sql = db_query($sql);

    if (db_affected_rows() > 0) {
      $results = array();
      while ($mid = db_result($sql)) {
        $mapping = new taxonomy_nodes_mapping();
        $mapping->get($mid);
        $results[] = $mapping;
      }
    }

    return $results;
  }



  public static function get_mapped_node_types() {
    $result = FALSE;
    $sql = "SELECT node_type FROM {taxonomy_nodes_mapping}";
    $sql = db_query($sql);

    if (db_affected_rows() > 0) {
      $results = array();
      while ($node = db_result($sql)) {
        $results[] = $node;
      }
    }

    return $results;
  }



  public static function get_mapped_vocabs($node_type = FALSE) {
    $result = FALSE;
    $sql = "SELECT vid FROM {taxonomy_nodes_mapping}";
    if ($node_type) {
      $sql .= " WHERE node_type = '%s'";
      $sql = db_query($sql, $node_type);
    }
    else {
      $sql = db_query($sql);
    }

    if (db_affected_rows() > 0) {
      $results = array();
      while ($vid = db_result($sql)) {
        $vocab = taxonomy_vocabulary_load($vid);
        $results[] = $vocab;
      }
    }

    return $results;
  }



  public static function exists($vid, $type, $id = NULL) {
    $mapping = new taxonomy_nodes_mapping();
    $mapping->vocab = $vid;
    $mapping->node = $type;
    $mapping->get_id();
    ($mapping->error || $mapping->id == $id)?$result = FALSE:$result = $mapping->id;
    return $result;
  }
}