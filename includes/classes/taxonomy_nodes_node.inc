<?php

/**
 * @file
 * Class handling all functionality pertaining to nodes - taxonomy
 */



define('LOADING_NO_RESULTS', 1);
define('SAVING_PARAMETERS_NOT_SET', 2);
define('CHECKING_PARAMETERS_NOT_SET', 3);
define('CHECKING_FAILED', 4);
define('DELETE_FAILED', 5);
define('UPDATE_FAILED', 6);


class taxonomy_nodes_node {
  public $nid;
  public $title;
  public $type;
  public $vocabs;
  public $error;



  public function  __construct() {
    $this->nid = FALSE;
    $this->title = FALSE;
    $this->type = FALSE;
    $this->vocabs = FALSE;
    $this->error = FALSE;
  }



  public function load($nid) {
    $node = node_load($nid);

    if ($node) {
      $this->nid = $nid;
      $this->title = $node->title;
      $this->type = $node->type;
      $vocabs = taxonomy_nodes_mapping::get_mapped_vocabs($this->type);
      if (count($vocabs) > 0) {
        $this->vocabs = array();
        foreach ($vocabs as $vocab) {
          $this->vocabs[] = $vocab->vid;
        }
      }
    }
    else $this->error = LOADING_NO_RESULTS;
  }



  public function add() {
    if ($this->vocabs) {
      foreach ($this->vocabs as $vid) {
        $mapping_exists = taxonomy_nodes_mapping::exists($vid, $this->type);
        if ($mapping_exists) {
          $this->insert($vid);
        }
      }
    }
  }



  public function update($new_title) {
    if ($this->vocabs) {
      foreach ($this->vocabs as $vid) {
        $this->update_vocab($vid, $new_title);
      }
    }
  }



  public function delete($vid = FALSE) {
    if ($this->vocabs) {
      if (!$vid) {
        foreach ($this->vocabs as $vid) {
          $this->delete_from_vocab($vid);
        }
        if ($this->error) {
          $this->error = DELETE_FAILED;
        }
      }
      else {
        $this->delete_from_vocab($vid);
        if ($this->error) {
          $this->error = DELETE_FAILED;
        }
      }
    }
  }



  private function delete_from_vocab($vid) {
    $sql = "SELECT tid FROM {term_data} WHERE vid = %d AND name = '%s'";
    $sql = db_query($sql, $vid, $this->title);
    if ($sql) {
      $tid = db_result($sql);

      $tables = array('term_data', 'term_hierarchy', 'term_node', 'term_synonym');

      foreach ($tables as $table) {
        $del = "DELETE FROM {%s} WHERE tid = %d";
        db_query($del, $table, $tid);
      }

      $del = "DELETE FROM {term_relation} WHERE tid1 = %d OR tid2 = %d";
      db_query($del, $tid, $tid);
    }
    else $this->error = CHECKING_FAILED;
  }



  private function exists_in_vocab($vid) {
    $result = FALSE;
    if ($this->title) {
      $sql = "SELECT * FROM {term_data} WHERE vid = %d AND name = '%s'";
      if (db_query($sql, $vid, $this->title)) {
        if (db_affected_rows() > 0) $result = TRUE;
      }
      else $this->error = CHECKING_FAILED;
    }
    else $this->error = CHECKING_PARAMETERS_NOT_SET;
    return $result;
  }



  private function insert($vid) {
    if ($this->title) {
      if (!$this->exists_in_vocab($vid)) {
        $insert_data = "INSERT INTO {term_data} (vid, name, description) VALUES (%d, '%s', '')";
        $insert_hierarchy = "INSERT INTO {term_hierarchy} (tid) VALUES(%d)";

        db_query($insert_data, $vid, $this->title);
        $id = db_last_insert_id('term_data', 'tid');
        db_query($insert_hierarchy, $id);
      }
    }
    else $this->error = SAVING_PARAMETERS_NOT_SET;
  }



  private function update_vocab($vid, $new_title) {
    $sql = "UPDATE {term_data} SET name = '%s' WHERE name = '%s' AND vid = %d";
    if (!db_query($sql, $new_title, $this->title, $vid)) $this->error = UPDATE_FAILED;
  }



  public static function get_nodes($node_type) {
    $select = "SELECT nid FROM {node} WHERE type = '%s'";
    $sql = db_query($select, $node_type);

    $nodes = array();

    while ($node = db_result($sql)) {
      $perm = 'create ' . $node->type . ' content';
      if (taxonomy_nodes_node::check_perms($perm)) {
        $nodes[] = node_load($node);
      }
    }

    return $nodes;
  }



  public static function check_perms($perm) {
    global $user;

    if ($user->uid == 1) { // Bypass, in case it's root user
      return TRUE;
    }
    else {
      $roles = user_roles(FALSE, $perm);
      $ok = array_intersect($roles, $user->roles);
      return (empty($ok)) ? FALSE : TRUE;
    }
  }



  public static function delete_from_mapping($vid, $node_type) {
    $nodes = taxonomy_nodes_node::get_nodes($node_type);

    foreach ($nodes as $node) {
      $tax_node = new taxonomy_nodes_node();
      $tax_node->load($node->nid);
      $tax_node->delete($vid);
    }
  }



  public static function add_from_mapping($vid, $node_type) {
    $nodes = taxonomy_nodes_node::get_nodes($node_type);

    foreach ($nodes as $node) {
      $tax_node = new taxonomy_nodes_node();
      $tax_node->load($node->nid);
      $tax_node->vocabs[] = $vid;
      $tax_node->add();
    }
  }



  public static function uninstall() {
    $mappings = taxonomy_nodes_mapping::get_all();

    foreach ($mappings as $mapping) {
      taxonomy_nodes_node::delete_from_mapping($mapping->vocab, $mapping->node);
    }
  }

}