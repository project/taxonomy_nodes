<?php

/**
 * @file
 * Contains all forms and their validation / submit callbacks
 */



/**
 * Form builder
 */
function taxonomy_nodes_forms($form_id, $args) {
  if (strpos($form_id, 'taxonomy_nodes_del_mapping_form') !== FALSE) {
    $forms[$form_id] = array('callback' => 'taxonomy_nodes_del_mapping_form');
    return $forms;
  }
}



/**
 * Form for new mappings.
 */
function taxonomy_nodes_mapping_form($form_state) {
  // Gets all node types the current user has permission for
  $node_types = node_get_types('names');
  foreach ($node_types as $type => $name) {
    $perm = 'create ' . $type . ' content';
    if (taxonomy_nodes_node::check_perms($perm)) {
      $nodes[$type] = $name;
    }
  }

  // Gets all vocabs
  $vocabs = taxonomy_get_vocabularies();
  $vocab_options = array();
  foreach ($vocabs as $vocab) {
    $vocab_options[$vocab->vid] = $vocab->name;
  }

  $form['node'] = array(
    '#type' => 'select',
    '#options' => $nodes,
    '#title' => t('Node type'),
  );

  $form['vocab'] = array(
    '#type' => 'select',
    '#options' => $vocab_options,
    '#title' => t('Vocabulary'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  $ok = TRUE;

  if (count($vocab_options) == 0) {
    $txt = t('No vocabularies found');
    $form['vocab']['#options'] = array($txt);
    $ok = FALSE;
  }

  if (count($nodes) == 0) {
    $txt = t('No node types found');
    $form['node']['#options'] = array($txt);
    $ok = FALSE;
  }

  if (!$ok) {
    $form['vocab']['#disabled'] = TRUE;
    $form['submit']['#disabled'] = TRUE;
    $form['node']['#disabled'] = TRUE;
  }

  return $form;
}



function taxonomy_nodes_mapping_form_validate($form, &$form_state) {
  $vals = $form_state['values'];
  $mapping_exists = taxonomy_nodes_mapping::exists($vals['vocab'], $vals['node']);
  if ($mapping_exists) {
    form_set_error('vocab', t("This mapping already exists."));
    form_set_error('node', t("This mapping already exists."));
  }
}



function taxonomy_nodes_mapping_form_submit($form, &$form_state) {
  $vid = $form_state['values']['vocab'];
  $node = $form_state['values']['node'];

  $mapping = new taxonomy_nodes_mapping();
  $mapping->node = $node;
  $mapping->vocab = $vid;
  $vocab = taxonomy_vocabulary_load($vid);
  $mapping->save();

  taxonomy_nodes_node::add_from_mapping($vid, $node);

  ($mapping->error)?$msg = 'Something went wrong adding !node_type - !vocab':$msg = '!node_type - !vocab has been added';
  $params = array('!node_type' => $node, '!vocab' => $vocab->name);
  drupal_set_message(t($msg, $params));
}



/**
 * Form for editing / deleting existing mappings
 */
function taxonomy_nodes_del_mapping_form($form_state, $mid) {
  $mapping = new taxonomy_nodes_mapping();
  $mapping->get($mid);

  $vocab = taxonomy_vocabulary_load($mapping->vocab);
  if (empty($vocab->name)) {
    $vocab->name = t('This vocabulary has been deleted');
  }

  $form['mid'] = array(
    '#type' => 'value',
    '#value' => $mid,
  );

  $form['node_item'] = array(
    '#type' => 'item',
    '#title' => t('Node type'),
    '#value' => $mapping->node,
    '#theme' => 'taxonomy_nodes_form_del_item',
  );

  $form['node'] = array(
    '#type' => 'value',
    '#value' => $mapping->node,
  );

  $form['vocab_item'] = array(
    '#type' => 'item',
    '#title' => t('Vocabulary'),
    '#value' => $vocab->name,
    '#theme' => 'taxonomy_nodes_form_del_item',
  );

  $form['vocab'] = array(
    '#type' => 'value',
    '#value' => $vocab->name,
  );

  $form['vid'] = array(
    '#type' => 'value',
    '#value' => $vocab->vid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  $form['#validate'][] = 'taxonomy_nodes_del_mapping_form_validate';
  $form['#submit'][] = 'taxonomy_nodes_del_mapping_form_submit';

  return $form;
}



function taxonomy_nodes_del_mapping_form_submit($form, &$form_state) {
  $mid = $form_state['values']['mid'];
  $node = $form_state['values']['node'];
  $vocab = $form_state['values']['vocab'];
  $vid = $form_state['values']['vid'];

  $mapping = new taxonomy_nodes_mapping();
  $mapping->get($mid);

  taxonomy_nodes_node::delete_from_mapping($vid, $node);
  $mapping->delete();
  ($mapping->error)?$msg = 'Something went wrong deleting !node_type - !vocab':$msg = '!node_type - !vocab has been deleted';
  $params = array('!node_type' => $node, '!vocab' => $vocab);
  drupal_set_message(t($msg, $params));
}