<?php

/**
 * @file
 * Theme functions
 */

/**
 * Theme for the admin page
 * @param string $add_form Form for adding a new mapping
 * @param string $edit_form Combination of all Edit forms
 * @return string
 */
function theme_taxonomy_nodes_admin($add_form, $edit_form) {
  $path = drupal_get_path('module', 'taxonomy_nodes');
  $add_title = t('Add');

  $return .= '<div id="taxonomy_nodes_wrapper">';

  if ($edit_form) {
    $return .= '<div class="taxonomy_nodes">' . $edit_form . '</div>';
    $return .= '<hr />';
  }

  $return .= '<h1>' . $add_title . '</h1>';
  $return .= '<div class="taxonomy_nodes">' . $add_form . '</div>';
  $return .= '</div>';
  return $return;
}



function theme_taxonomy_nodes_form_del_item($form) {
  $output = '<div class="form-item taxonomy_nodes_del_item">';
  $output .= '<label>' . $form['#title'] . '</label>';
  $output .= $form['#value'];
  $output .= '</div>';
  return $output;
}
