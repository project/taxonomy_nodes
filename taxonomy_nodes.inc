<?php

/**
 * @file
 * Makes sure all necessary files are included.
 * Only include this file when necessary
 */

$path = drupal_get_path('module', 'taxonomy_nodes');
require_once($path . '/includes/classes/taxonomy_nodes_mapping.inc');
require_once($path . '/includes/classes/taxonomy_nodes_node.inc');
require_once($path . '/includes/theme/taxonomy_nodes_theme.inc');
require_once($path . '/includes/taxonomy_nodes_forms.inc');